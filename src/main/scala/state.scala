package rogue

/// Represents state actions handled by the StateReducer
sealed trait Message

/// Creates a player in the state at pos
final case class SpawnPlayer(pos: Pos) extends Message

/// Adds an entity to the state, does not check if position is empty
final case class AddEntity(entity: Entity, pos: Pos) extends Message

/// Moves the entity by delta not checking occupancy
final case class MoveEntity(entity: Entity, delta: Pos) extends Message

// TODO Use an item from the entity's inventory
final case class UseItem(entity: Inventory, itemName: String) extends Message

final case class State(state: Map[Entity, Pos]) {
  /// Gets the first Player entity
  def getPlayer = this.state.keys.find { case player: Player => true; case _ => false }
  
  /// value -> [key]
  def inverse = this.state.keys.groupBy(this.state)
  
  def setEntity(entity: Entity, pos: Pos) = State(this.state + (entity -> pos))
  
  def removeEntity(entity: Entity) = State(this.state - entity)

  def mkBoard(renderdistance: Int):Either[String,Board[Drawable]] ={
    val pelaaja = this.getPlayer
    if(pelaaja == None){return Left("No player created yet")}

    var b:Board[Drawable] = Board.empty(2*renderdistance+1,2*renderdistance+1)
    val playerPos = this.state(pelaaja.get)
    val origin = Pos(renderdistance,renderdistance)
    for((e, pos) <- this.state){
      e match {
        case e: Drawable => {
          val boardpos = pos - playerPos + origin
          pos - playerPos match {
            case Pos(x,y) if(Math.max(Math.abs(x),Math.abs(y))>renderdistance) => 
            case _ => b=b.update(boardpos, e)
          }
        }
        case _ => 
      }
    }
    
    Right(b)
  } 
}

object State {
  def empty = State(Map())
}

object StateReducer {
  def send(message: Message)(state: State): Either[String, State] = {
    message match {
      case SpawnPlayer(pos) =>
      state.getPlayer match {
        case None => Right(state.setEntity(Player.noob, pos))
        case _ => Left("Player already spawned")
      }
      case AddEntity(entity: Player, _) => Left("Use `SpawnPlayer` for adding the player")
      case AddEntity(entity, pos) => Right(state.setEntity(entity, pos))
      case MoveEntity(entity, delta) =>
      state.state.get(entity) match {
        case None => Left("Cannot move: no entity")
        case Some(pos) => {
          val targetPos = pos + delta
          Right(state.setEntity(entity, targetPos))
        }
      }
      case UseItem(entity, itemName) =>
      entity
      .useItem(itemName, entity)
      .map(newEntity =>
      state
      .removeEntity(entity)
      .setEntity(newEntity, state.state(entity))
      )
    }
  }
}

/// Represents a grid of elements
case class Board[+T<:Drawable](val width: Int, val elements: Vector[Option[T]]) {
  def height = this.elements.length / this.width
  
  def apply(pos: Pos):Option[T] = this.elements.lift(pos.toIndex(this.width)) match {
    case None => None 
    case Some(value) => value
  }
  
  def positionOf[U >: T <: Drawable](element: U): Option[Pos] = {
    val ind = this.elements.indexOf(Some(element))
    ind match {
      case -1 => None
      case ind => Some(Pos(ind % this.width, ind / this.width))
    }
  }
  
  def update[U >: T <: Drawable](pos: Pos, element: U): Board[U] = Board(
    this.width,
    this.elements.updated(pos.toIndex(this.width), Some(element))
  )
  
  
  def toCharVec:Vector[Vector[Char]] =
    this.elements.sliding(this.width,this.width).map(row => row.map(elementti => elementti match{
      case None => '.'
      case Some(value) => value.toCharacter 
    })).toVector
    

  def display:String = {
    val laaatikko = Vector(" "*(width+2))
    val rivit = this.toCharVec.map(row => s" ${row.mkString} ")
    (laaatikko ++ rivit ++ laaatikko).mkString("\n")
  }

  override def toString = this.display
}

object Board {
  def empty[T <: Drawable](width: Int, height: Int) = Board(width, Vector.fill(width * height)(None: Option[T]))
}
