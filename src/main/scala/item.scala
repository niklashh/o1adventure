package rogue

sealed trait Item {
  val name: String
  /// Left is reason for why cannot use, Right is the updated Entity
  def use(entity: Entity): Either[String, Entity]
}

trait Weapon {
  val damage: Int
}

trait Food {
  /// Amount of health added
  val nutrients: Int

  /// Left is reason for why cannot eat, Right means can eat
  def canEat: Either[String, Unit]
}

final case class UnbakedPizza(nutrients: Int) extends Item with Weapon with Food {
  val name = "Unbaked delicious pizza"
  def use(entity: Entity) = Left("Cannot use unbaked pizza")

  val damage = 2
  val canEat = Left("Pizza is unbaked")

  def bake: BakedPizza = BakedPizza(this.nutrients)
}

final case class BakedPizza(nutrients: Int) extends Item with Weapon with Food {
  val name = "Fresh delicious baked pizza"
  def use(entity: Entity) = entity match {
    case player: Player => Right(Player(player.hp + this.nutrients, player.inventory.diff(Vector(this))))
    case entity => Left(s"Cannot use BackedPizza on ${entity}")
  }
  val damage = 2
  val canEat = Right(())
}

// final case class Sword() extends Item[Entity] with Weapon {
//   val name = "A mighty flaming laser sword"
//   def use[T<:Entity](entity: T) = entity match {
//     case injurable: Injurable => Right(injurable.injure(this.damage))
//     case _ => Left(s"Cannot use sword on uninjurable ${entity.name}")
//   }
//   val damage = 12
// }
