package rogue

package object util {
  case class Pos(val x: Int, val y: Int) {
    def +(other: Pos) = Pos(this.x + other.x, this.y + other.y)

    def unary_- = Pos(-this.x, -this.y)
    def -(other: Pos) = this + -other

    def toIndex(width: Int) = this.x + width * this.y
  }
}
