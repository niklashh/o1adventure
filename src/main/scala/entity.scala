package rogue

sealed trait Entity {
  val name: String
}

trait Drawable {
  val toCharacter: Char
} 

trait Injurable extends Entity {
  def injure(amount: Int): Injurable
}

trait Inventory extends Entity {
  val inventory: Vector[Item]

  def addItem(item: Item): Inventory

  def getItem(name: String): Option[Item] = this.inventory.find(_.name==name)

  def useItem(name: String, on: Entity): Either[String, Entity] = this.getItem(name) match {
    case None => Left(s"No such item: ${name}")
    case Some(item) => item.use(on)
  }

  def useOnSelf(name: String) = this.useItem(name, this)
}

final case class Wall() extends Entity{
  val name = "wall"
}

final case class Player(hp: Int, inventory: Vector[Item]) extends Entity with Injurable with Inventory with Drawable {
  val name = "player"

  val toCharacter: Char = 'P'

  def addItem(item: Item) = PlayerBuilder.from(this).addItem(item).build

  def attack = ???

  def injure(amount: Int) = PlayerBuilder.from(this).hp(this.hp - amount).build
}

object Player{
  def noob = Player(100,Vector())
}

final case class PlayerBuilder() {
  private var hp = 100
  private var inventory = Vector[Item]()

  def hp(hp: Int): PlayerBuilder = {
    this.hp = hp
    this
  }

  def inventory(inventory: Vector[Item]): PlayerBuilder = {
    this.inventory = inventory
    this
  }

  def addItem(item: Item) = {
    this.inventory = this.inventory :+ item
    this
  }

  def build = Player(hp, inventory)
}

object PlayerBuilder {
  def from(player: Player) = {
    PlayerBuilder().hp(player.hp).inventory(player.inventory)
  }
}

final case class Enemy(name: String, hp: Int) extends Entity with Injurable {
  def injure(amount: Int) = Enemy(this.name, this.hp - amount)
}

trait ArtificialIntelligence {
  // TODO either
  def tick(state: State): Vector[Message]
}

final case class IntelligentEnemy(name: String, hp: Int) extends Entity with Injurable with ArtificialIntelligence with Drawable {

  val toCharacter: Char = 'E'
  
  def injure(amount: Int) = Enemy(this.name, this.hp - amount)

  def tick(state: State) = {
    (state.getPlayer, state.state.get(this)) match {
      case (Some(player), Some(pos)) => {
        val delta = state.state(player) - pos

        println(delta)
        val direction = (math.signum(delta.x), math.signum(delta.y)) match {
          case (-1, _) => Pos(-1, 0)
          case (1, _) => Pos(1, 0)
          case (_, -1) => Pos(0, -1)
          case (_, 1) => Pos(0, 1)
          case (_, _) => Pos(0, 0) // should probably attack before walking over the player
        }

        Vector(MoveEntity(this, direction))
      }
      case (_, _) => Vector()
    }
  }
}
