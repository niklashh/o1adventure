package rogue

import scala.io.StdIn._

object peli extends App {
    val tyhja = State.empty
    val alkutila = Vector[Message](SpawnPlayer(Pos(0,0)),AddEntity(Enemy("enemy",100),Pos(3,3)))
    def gametick(state:State, kasa:Vector[Message])= {
        kasa.foldLeft(state)((edellinen,message)=>StateReducer.send(message)(edellinen) match{
            case Left(error) => {
                println(error)
                edellinen
            }
            case Right(state) => state
        })
    }
    
    var stop = false
    var state = gametick(tyhja, alkutila)

    while(!stop){
        val playerMessages = readLine("=> ") match{
            case "left" => Vector(MoveEntity(state.getPlayer.get, Pos(-1,0)))
            case "right" => Vector(MoveEntity(state.getPlayer.get, Pos(1,0)))
            case "up" => Vector(MoveEntity(state.getPlayer.get, Pos(0,1)))
            case "down" => Vector(MoveEntity(state.getPlayer.get, Pos(0,-1)))
            case "intelligent enemy" => Vector(AddEntity(IntelligentEnemy("pahis", 50), Pos(1,1)))
            case _ => Vector()
        }
        var allMessages = state.state.keys.flatMap{
            case ai:ArtificialIntelligence => ai.tick(state)
            case _ => Vector()
        }   
        
        println(allMessages)
        state = gametick(state, playerMessages ++ allMessages)
        println(state)








    }

     
    
    
    
    
    
}