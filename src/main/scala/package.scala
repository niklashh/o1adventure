package object rogue {
  type Pos = util.Pos
  val Pos = util.Pos

  type Try[T] = scala.util.Try[T]
  val Try = scala.util.Try

  type Success[T] = scala.util.Success[T]
  val Success = scala.util.Success

  type Failure[T] = scala.util.Failure[T]
  val Failure = scala.util.Failure
}
