package rogue

import org.scalatest._
import flatspec._
import matchers._

class BoardSpec extends AnyFlatSpec {
  it should "create a new board" in {
    val b = Board(1, Vector(Some(Player.noob), Some(Player.noob)))
    assert(b.height == 2)
  }

  behavior of "apply"

  it should "get existing element" in {
    val vihollinen = IntelligentEnemy("kissa", 7)
    val b = Board(2, Vector(Some(Player.noob), Some(vihollinen)))
    // ....
    // .PE.
    // ....

    assert(b(Pos(1, 0)) == Some(vihollinen))
  }

  it should "get none outside" in {
    val b = Board(2, Vector(Some(Player.noob), Some(Player.noob)))
    // ....
    // .PP.
    // ....

    assert(b(Pos(999, 999)) == None)
  }

  behavior of "toString"

  it should "return a string that describes the board" in {
    val b = Board(2, Vector(Some(Player.noob), Some(Player.noob), Some(Player.noob), None))
    
    // PP
    // P.
    
    assert("    \n PP \n P. \n    " == b.toString)
}}

// board -> String
