package rogue

import org.scalatest._
import flatspec._
import matchers._

// class PlayerSpec extends AnyFlatSpec {
//   it should "create a new player" in {
//     val p = Player
//   }
// }

class StateSpec extends AnyFlatSpec {
  it should "create a new state" in {
    val s = State.empty
    assert(s == State(Map()))
  }

  behavior of "SpawnPlayer"

  it should "create a new player at 1, 0" in {
    val s = State.empty
    assert(StateReducer.send(SpawnPlayer(Pos(1, 0)))(s) ==
      Right(State(Map(Player.noob -> Pos(1, 0)))))
  }

  behavior of "AddEntity"

  it should "create a new enemy at 2, 2" in {
    val s = State.empty
    val enemy = new Enemy("kana", 10)
    val message = AddEntity(enemy,Pos(2,2))
    val nextState = StateReducer.send(message)(s)
    val wantedState = Right(State(Map(enemy -> Pos(2,2))))
    assert(nextState==wantedState)
  }

  behavior of "BakedPizza"

  it should "when used, give health to Player" in {
    val pizza = BakedPizza(10)
    val p = Player(100, Vector(pizza))
    val result = p.useOnSelf("Fresh delicious baked pizza")
    assert(result == Right(Player(110, Vector())))
  }

  it should "fail to use on Enemy" in {

  }

  it should "only consume one item from inventory" in {

  }

  behavior of "UnbakedPizza"

  it should "fail to use" in {
    val pizza = UnbakedPizza(10)
    val p = Player(100, Vector(pizza))
    val result = p.useOnSelf("Unbaked delicious pizza")
    assert(result == Left("Cannot use unbaked pizza"))
  }

  behavior of "injure"

  it should "reduce Player's hp" in {
    val p = Player.noob
    val result = p.injure(100)
    assert(result == Player(0, Vector()))
  }

  it should "reduce Enemy's hp" in {
    val e = Enemy("enemy", 10)
    val result = e.injure(5)
    assert(result == Enemy("enemy", 5))
  }

  behavior of "IntelligentEnemy"

  it should "move towards the player" in {
    val enemy = IntelligentEnemy("pizzamonster9000", 9000)
    // #######
    // #     #
    // #  P  #
    // #     #
    // #     #
    // #    E#
    // #######
    var s = State(
      Map(
        enemy -> Pos(5, 5),
        Player.noob -> Pos(3, 2)
      )
    )

    def tick(s: State) = enemy
      .tick(s)
      .foldLeft(Right(s): Either[String, State])((state, message) =>
        state.flatMap(StateReducer.send(message) _)
      )

    // #######
    // #     #
    // #  P  #
    // #     #
    // #     #
    // #   Ee#
    // #######
    var nextState = tick(s)
    assert(nextState==Right(State(Map(
      enemy -> Pos(4, 5),
      Player.noob -> Pos(3, 2)
    ))))

    // #######
    // #     #
    // #  P  #
    // #     #
    // #     #
    // #  Ee #
    // #######
    nextState = nextState.flatMap(tick)
    assert(nextState==Right(State(Map(
      enemy -> Pos(3, 5),
      Player.noob -> Pos(3, 2)
    ))))

    // #######
    // #     #
    // #  P  #
    // #     #
    // #  E  #
    // #  e  #
    // #######
    nextState = nextState.flatMap(tick)
    assert(nextState==Right(State(Map(
      enemy -> Pos(3, 4),
      Player.noob -> Pos(3, 2)
    ))))
  }

    // s = s.setPlayer(Pos(0,0))
    // println(s)
    // s = StateReducer.send(s)(AddEntity(new Enemy, Pos(1,0))).get
    // s = StateReducer.send(s)(AddEntity(new Enemy, Pos(2,0))).get
    // println(s)
    // val result = StateReducer.send(s)(MovePlayer(Pos(1,0)))
    // println(result)
    // s = StateReducer.send(s)(MovePlayer(Pos(0,1))).get
    // println(s)
}
