pub type HP = i32;
pub type Pos = (i32, i32);

pub trait GameObject: Sized {
    fn pos(&self) -> Pos;
    fn hp(&self) -> HP;
}

pub trait Movable: GameObject {
    fn add_x(self, amount: i32) -> Self {
        let (x, y) = self.pos();
        self.into_new_pos((x + amount, y))
    }
    fn add_y(self, amount: i32) -> Self {
        let (x, y) = self.pos();
        self.into_new_pos((x, y + amount))
    }

    /// the implementor must describe how the new position will be
    /// applied to self
    fn into_new_pos(self, pos: Pos) -> Self;
}

pub type RgbTuple = (u8, u8, u8);

pub trait Color {
    fn as_rgb_tuple(&self) -> RgbTuple;
}

impl Color for RgbTuple {
    fn as_rgb_tuple(&self) -> RgbTuple {
        *self
    }
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Debug)]
pub enum Colors {
    RED,
    GREEN,
    BLUE,
    BLACK,
    WHITE,
}

impl Color for Colors {
    fn as_rgb_tuple(&self) -> RgbTuple {
        use Colors::*;
        match *self {
            RED => (255, 0, 0),
            GREEN => (0, 255, 0),
            BLUE => (0, 0, 255),
            BLACK => (0, 0, 0),
            WHITE => (255, 255, 255),
        }
    }
}

pub trait CharDraw: GameObject {
    fn color(&self) -> RgbTuple;
    fn draw(&self, screen: &mut Screen) {
        screen.buf[self.pos().1 as usize * screen.width + self.pos().0 as usize] = self.color();
    }
}

pub struct Screen {
    width: usize,
    buf: Vec<RgbTuple>,
}

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Player {
    pos: Pos,
    hp: HP,
}

impl Player {
    fn new(pos: Pos, hp: HP) -> Self {
        Self { pos, hp }
    }
}

impl GameObject for Player {
    fn pos(&self) -> Pos {
        self.pos
    }
    fn hp(&self) -> HP {
        self.hp
    }
}

impl Movable for Player {
    fn into_new_pos(self, pos: Pos) -> Self {
        Self { pos, hp: self.hp() }
    }
}

impl CharDraw for Player {
    fn color(&self) -> RgbTuple {
        Colors::BLUE.as_rgb_tuple()
    }
}

struct GameState {
    player: Player,
    // rng?
}

enum Event {
    KeyLeft,
    KeyRight,
    KeyUp,
    KeyDown,
}

pub type ShouldDraw = bool;

impl GameState {
    fn new() -> Self {
        Self {
            player: Player::new((0, 0), 100),
        }
    }

    fn draw(&self, screen: &mut Screen) {
        self.player.draw(screen);
    }

    fn handle_event(&mut self, input: Event) -> ShouldDraw {
        match input {
            Event::KeyLeft => {
                self.player = self.player.add_x(-1);

                true
            }
            _ => todo!(),
        }
    }

    fn get_player(&self) -> &Player {
        &self.player
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_create_player() {
        let p = Player::new((10, 10), 100);
        assert_eq!(p.pos(), (10, 10));
        assert_eq!(p.hp(), 100);
    }

    #[test]
    fn should_draw_player() {
        let black = (0, 0, 0);
        let mut s = Screen {
            width: 3,
            buf: std::iter::repeat(black).take(9).collect(),
        };
        let p = Player::new((1, 2), 100);
        p.draw(&mut s);

        assert_eq!(s.buf[7], Colors::BLUE.as_rgb_tuple());
    }

    #[test]
    fn should_draw_game() {
        let game = GameState::new();
        let black = (0, 0, 0);
        let mut s = Screen {
            width: 3,
            buf: std::iter::repeat(black).take(9).collect(),
        };
        game.draw(&mut s);
        assert_eq!(s.buf[0], Colors::BLUE.as_rgb_tuple());
    }

    #[test]
    fn should_handle_event() {
        let mut game = GameState::new();
        game.handle_event(Event::KeyLeft);
        assert_eq!(game.get_player().pos(), (-1, 0));
    }
}
