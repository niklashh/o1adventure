* Text terminal roguelike

** Game state

*** Entity

Everything in the state is an entity:
 - Player
 - Enemy
 - Wall

*** Messages

All state transforms are done by a reducer which eats messages:
 - SpawnPlayer
 - AddEntity
 - MovePlayer

*** Level

A level represents a map with walls, start point, enemies

The map is loaded from a png image

** Basic game mechanics

 - Health does not increase over time
 - Stamina can be increased by resting
 - Stamina is used for fighting
 - Money can be gotten from killing enemies or by opening treasures
 - Not turn based, the player and an enemy might move on the same tick
 - Limited visibility
 - Finding an exit loads the next level
 - Enemies move around randomly
 - Potions of health and stamina can be found. They are consumed once
   a subject moves to the same square

Undecided
 - key needed for some treasures?
 - key needed for some doors/exits?
 
** Fighting enemies

Enemies have pathfinding at some distance

If the player is within R distance (manhattan) of an enemy it can
attack all enemies within that range dealing equal damage to each

Enemies have a difficulty rating

** Objectives

 - Game state matrix representation
 - A modular system where a new game can be created just by modifying the matrices and mechanics
 - Different subjects
   - Player
   - Enemy
   - Treasure
   - Potion
 - Map image with colors representing some fields or walls
   * Map image could be generated randomly
 - Randomized enemy movement
 - Terminal based
 - Player has limited visibility
 - Objective of the game is to collect money and find an exit
   - After finding an exit the game will generate a more difficult level
   - Once the player dies, money is the final score
