ThisBuild / scalaVersion := "2.13.6"
Compile / unmanagedClasspath += baseDirectory.value / "lib"
ThisBuild / libraryDependencies += "org.scala-lang" % "scala-compiler" % scalaVersion.value
ThisBuild / libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.10"
ThisBuild / libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.10" % "test"

lazy val rogue = (project in file("."))
  .settings(
    name := "rogue",
    // Compile / unmanagedBase := baseDirectory.value / "../lib"
  )
